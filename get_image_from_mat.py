import numpy as np
import h5py
import cv2


from lib_csscnet.file_utils import get_file_prefixes_from_path
import numpy as np
from sklearn.model_selection import train_test_split
import os
from shutil import copyfile

MAT_FILE   = '/(...)/nyu-v2/nyu_depth_v2_labeled.mat'
TRAIN_PATH = '/(...)/NYU/NYUtrain'
VAL_PATH   = '/(...)/NYU/NYUtest'



train_prefixes = get_file_prefixes_from_path(TRAIN_PATH)
train_views = [int(x[-9:-9+4])-1 for x in train_prefixes]

val_prefixes = get_file_prefixes_from_path(VAL_PATH)
val_views = [int(x[-9:-9+4])-1 for x in val_prefixes]

f = h5py.File(MAT_FILE, 'r')
images = np.array(f['images'])
scenes = np.array(f['scenes'])

images = np.transpose(images,(0,3,2,1))

for idx, image in enumerate(images):
    h_scene = f[scenes[0, idx]]
    scene = ''.join(chr(i) for i in h_scene[:])
    print(scene)

    img = cv2.cvtColor(image,cv2.COLOR_RGB2BGR)
    if idx in train_views:
        img_file = os.path.join(TRAIN_PATH,'NYU%04d_0000_color.jpg' % (idx+1))
    else:
        img_file = os.path.join(VAL_PATH, 'NYU%04d_0000_color.jpg' % (idx + 1))
    cv2.imwrite(img_file,img)
    print(img_file)

