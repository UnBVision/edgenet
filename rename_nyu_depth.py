from lib_csscnet.file_utils import get_file_prefixes_from_path
import numpy as np
from sklearn.model_selection import train_test_split
import os

TRAIN_PATH = '/(...)/NYU/NYUtrain'
VAL_PATH   = '/(...)/NYU/NYUtest'



prefixes = get_file_prefixes_from_path(TRAIN_PATH, criteria='*.png') + get_file_prefixes_from_path(VAL_PATH, criteria='*.png')

for prefix in prefixes:
    os.rename(prefix+'.png', prefix+'_depth.png')
    print(prefix)
