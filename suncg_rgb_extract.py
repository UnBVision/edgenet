import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="1"
os.environ["KERAS_BACKEND"]="tensorflow"
from lib_csscnet.file_utils import get_file_prefixes_from_path
import numpy as np # linear algebra
import multiprocessing
import argparse
import time
import struct
import shutil

import cv2
import math


##################################
# Parameters
##################################

BASE_PATH = '/(...)/data/depthbin/SUNCGtest_49700_49884/'
TEMP_DIR = '/(...)/output'
suncgDataPath = '/(...)/data/suncg_v1/'
suncgToolboxPath = '/(...)/SUNCGtoolbox/'




usemeasa = ''
pathtogaps = os.path.join(suncgToolboxPath, 'gaps/bin/x86_64')

def process_scene(sceneId, cam_list, bin_list):

    cam_file = os.path.join(TEMP_DIR, 'cam/'+sceneId + '.cam')
    f = open(cam_file, 'w')
    for cp in cam_list:
        f.write("%f %f %f %f %f %f %f %f %f %f %f %f \n" %
                (cp[0], cp[1], cp[2], cp[3], cp[4], cp[5],
                 cp[6], cp[7], cp[8], cp[9], cp[10], cp[11])
                )
    f.close()

    projectpath = os.path.join(suncgDataPath, 'house/' + sceneId)


    cmd_gen_rgb = format('unset LD_LIBRARY_PATH;\n'
                         ' cd  %s \n %s/scn2img house.json '
                         ' %s -capture_color_images -xfov 0.55 %s %s' %
                         (projectpath, pathtogaps, cam_file, TEMP_DIR, usemeasa))
    ret = os.system(cmd_gen_rgb)

    #ret  = 0 #not to generate images
    if ret != 0:
        print("Error:", ret, sceneId)
    else:
        caminfo_file = os.path.join(TEMP_DIR, 'caminfo/'+sceneId + '.caminfo')
        f = open(caminfo_file, 'w')

        for n, bin_file in enumerate(bin_list):

            old_name = os.path.join(TEMP_DIR,str(n).zfill(6) + "_color.jpg")
            img_o = cv2.imread(old_name)
            img_f = cv2.flip(img_o, 1)
            cv2.imwrite(bin_file + "_color.jpg", img_f)

            floorId = int(bin_file[-15:-12])
            roomId = int(bin_file[-9:-5])
            f.write("Room#%d_%d_%d\n" % (floorId-1, roomId-1, 0))
            #print(bin_file,floorId,roomId)
        f.close()

file_prefixes = get_file_prefixes_from_path(BASE_PATH, criteria='*.bin')

print("Files to process:", len(file_prefixes))

sceneId_ant = ''
cam_list=[]
bin_list=[]

processed = 0

for file_prefix  in file_prefixes:
    sceneId = file_prefix[-50:-18]

    if processed % 15 == 0:
        print("Processed: %d (%.2f%%)        " % (processed, 100*processed/len(file_prefixes)), end="\r")

    if (sceneId != sceneId_ant):
        if len(cam_list) >0:
            process_scene(sceneId_ant, cam_list, bin_list)

        sceneId_ant = sceneId
        cam_list=[]
        bin_list=[]
        qty = 0

    qty += 1
    processed += 1
    f = open(file_prefix + '.bin', 'rb')
    vox_origin = np.array(struct.unpack("3f", f.read(3 * 4)))
    ex = np.array(struct.unpack("16f", f.read(16 * 4)))
    f.close()

    #campose
    cam_list.append([ex[3], ex[11], ex[7], ex[2], ex[10], ex[6], -ex[1], -ex[9], -ex[5], 0.55, 0.388863, 17.0])
    bin_list.append(file_prefix)

    sceneId = file_prefix[-50:-18]

process_scene(sceneId_ant, cam_list, bin_list)
print("Processed: %d (%.2f%%)        " % (processed, 100 * processed / len(file_prefixes)))

#outdir = os.path.join(DESTINATION_PATH,file_prefix[len(BASE_PATH):-50])
            # generating depth images
    #output_image_directory = os.path.join(outputdir, sceneId + '/images');




