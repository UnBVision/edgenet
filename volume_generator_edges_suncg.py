import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
os.environ["CUDA_VISIBLE_DEVICES"]="0"
os.environ["KERAS_BACKEND"]="tensorflow"
import sys

from lib_csscnet.file_utils import *
from lib_csscnet.metrics import *
from lib_csscnet.losses import *

import numpy as np # linear algebra
from keras.optimizers import SGD

from lib_csscnet.network import *

import h5py

segmentation_class_map = np.array([0, 1, 2, 3, 4, 11, 5, 6, 7, 8, 8, 10, 10, 10, 11, 11, 9, 8, 11, 11,
                                   11, 11, 11, 11, 11, 11, 11, 10, 10, 11, 8, 10, 11, 9, 11, 11, 11], dtype=np.int32)

##################################

full_sh = (240, 144, 240)
down_sh = (60, 36, 60)
qty = 6

class_names = ["ceil.", "floor", "wall ", "wind.", "chair", "bed  ", "sofa ", "table", "tvs  ", "furn.", "objs."]

#data_path = '/vol/vssp/datasets/multiview/SUNCG/PreprocessedData/edges/SUNCGtest_49700_49884'
data_path = '/d02/data/csscnet_edges_preproc/SUNCGtest_49700_49884'

file_prefixes = get_file_prefixes_from_path(data_path, criteria='*.npz')

print(file_prefixes[:7])


file_prefixes = [x for x in file_prefixes if os.path.basename(x) in [
        '00000032_e7239f75c773111fb52a7468bfa83682_fl001_rm0001_0000',
        '00000040_e6668bda782f3dde4875e4f22ccc778a_fl001_rm0002_0000',
        '00000097_e66305ae98faab6b809ed2b2cbe82bed_fl001_rm0004_0000',
        '00000155_e6c28aa57a0026832d208e5713af29f1_fl001_rm0014_0000',
        '00000156_e6c28aa57a0026832d208e5713af29f1_fl001_rm0019_0000',
        '00000199_e6e076af64dad9743188594ce414e035_fl001_rm0002_0000',
        '00000421_e67adda6ef2f67fc30580840e563b16f_fl001_rm0001_0000']]

print(file_prefixes)


#file_prefixes = [x for x in file_prefixes if os.path.basename(x) in ['NYU0959_0000', 'NYU0638_0000',
#                                                                     'NYU1202_0000', 'NYU1337_0000',
#                                                                     'NYU1314_0000', 'NYU0524_0000',
#                                                                     'NYU1395_0000']]

#datagen = preproc_generator(file_prefixes, batch_size=1, shuff=False, shape=(240, 144, 240), type="edges", vol=True)
datagen = preproc_generator(file_prefixes, batch_size=1, shuff=False, shape=(240, 144, 240), type="depth", vol=True)


'''
#GROUND TRUTH#
for i,file_prefix in enumerate(file_prefixes):

    print("Generating volumes for ", file_prefix)

    x, y, vol = next(datagen)

    vox_tsdf = x[0][0]
    vox_edges = x[1][0]

    segmentation_label = np.argmax((y[0]>=1).astype(int), axis=-1)
    vox_weights = np.max(y[0]-1, axis=-1)

    vox_vol = vol[0]

    print(y[0].shape)
    print(segmentation_label.shape)

    vox_vol[vox_vol==0] = 10

#    print("myvol")
#    voxel_export("./blender/myvol"+str(i)+".bin", vox_vol, shape=(60, 36, 60))
    print("mygt")
    voxel_export("./blender/suncggt"+str(i)+".bin", segmentation_label, shape=(60, 36, 60))
#    print("mytfds")
#    voxel_export("/home/adn/unb/colour-sscnet/blender/mytsdf"+str(i)+".bin", vox_tsdf, shape=(240, 144, 240))
#    print("myedges")
#    voxel_export("/home/adn/unb/colour-sscnet/blender/myedges"+str(i)+".bin", vox_edges, shape=(240, 144, 240))
#   print("myw")
#    voxel_export("/home/adn/unb/colour-sscnet/blender/myw"+str(i)+".bin", vox_weights, shape=(60, 36, 60))

'''
#model = get_res_unet_edges()
#model = get_sscnet_edges()
#model = get_sscnet()
model = get_res_unet()

model.compile(optimizer=SGD(lr=0.01, decay=0.005,  momentum=0.9),
              loss=weighted_categorical_crossentropy
              ,metrics=[comp_iou, seg_iou]
              )

#model_name = os.path.join('./weights','R_UNET_E_LR0.002_DC0.0005_209-0.73-0.74.hdf5')
#model_name = os.path.join('./weights','SSCNET_E_LR0.01_DC0.0005_3524-0.69-0.70.hdf5')
#model_name = os.path.join('./weights','SSCNET_LR0.01_DC0.0005_3926-0.68-0.70.hdf5')
model_name = os.path.join('./weights','R_UNET_LR0.005_DC0.0005_508-0.74-0.74.hdf5')
model.load_weights(model_name)


for i,file_prefix in enumerate(file_prefixes):

    print("Generating volumes for ", file_prefix)

    x, y, vol = next(datagen)
    pred = model.predict(x=x)
    weights = np.amax(y - 1,axis=-1, keepdims=False)
    flags = np.array((weights!=0),dtype='float32')
    y_true = np.argmax(y, axis=-1) * flags
    y_pred = np.argmax(pred, axis=-1) * flags

#    voxel_export("./blender/suncg_R_UNET_E"+str(i)+".bin", y_pred, shape=(60, 36, 60))
#    print("nyu_R_UNET_E_Pred")
    voxel_export("/home/adn/unb/colour-sscnet/blender/suncg_R_UNET" + str(i) + ".bin", y_pred, shape=(60, 36, 60))
    print("nyu_R_UNET_Pred")
#    voxel_export("/home/adn/unb/colour-sscnet/blender/suncg_SSCNET_E" + str(i) + ".bin", y_pred, shape=(60, 36, 60))
#    print("suncg_SSCNET_E_Pred")
#    voxel_export("/home/adn/unb/colour-sscnet/blender/suncg_SSCNET" + str(i) + ".bin", y_pred, shape=(60, 36, 60))
#    print("suncg_SSCNET_Pred")
#    voxel_export("/home/adn/unb/colour-sscnet/blender/nyu_label" + str(i) + ".bin", y_pred, shape=(60, 36, 60))
#    print("nyu_UNET_Pred")
#    voxel_export("/home/adn/unb/colour-sscnet/blender/mytsdf"+str(i)+".bin", vox_tsdf, shape=(240, 144, 240))
#    print("myedges")
#    voxel_export("/home/adn/unb/colour-sscnet/blender/myedges"+str(i)+".bin", vox_edges, shape=(240, 144, 240))
#   print("myw")
#    voxel_export("/home/adn/unb/colour-sscnet/blender/myw"+str(i)+".bin", vox_weights, shape=(60, 36, 60))

